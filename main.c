#include "Rii.h"
//Para debuger el programa usar los siguientes comandos
//  gcc GrafoSt.c hash.c orden.c input.c main.c -g -o run
//  ./run < Tests/3_edges"
//  valgrind --leak-check=full -v ./run


int main(){   
    clock_t t_ini, t_fin;
    double secs;

    t_ini = clock();
    Grafo G = ConstruccionDelGrafo();
    t_fin = clock();
    secs = (double)(t_fin - t_ini) / CLOCKS_PER_SEC;
    printf("\n%.16g segundos de Todo\n\n", secs);
    (check_colors(G) == 1) ? printf("Esta BIEN coloreado\n") : printf("Esta MAL coloreado\n");  

    //Grafo G2 = CopiarGrafo(G);
    t_ini = clock();
    OrdenNatural(G);
    t_fin = clock();
    secs = (double)(t_fin - t_ini) / CLOCKS_PER_SEC;
    printf("\n%.16g Ordenar\n\n", secs);
    t_ini = clock();
    Greedy(G);
    t_fin = clock();
    secs = (double)(t_fin - t_ini) / CLOCKS_PER_SEC;
    printf("\n%.16g segundos de Greedy\n\n", secs);
    printf("\nGreedy En orden de Natural: %u \n", G->nc);
    (check_colors(G) == 1) ? printf("Esta BIEN coloreado\n") : printf("Esta MAL coloreado\n");  

    OrdenWelshPowell(G);
    Greedy(G);
    printf("\nGreedy En orden de WellPower: %u \n", G->nc);
    (check_colors(G) == 1) ? printf("Esta BIEN coloreado\n") : printf("Esta MAL coloreado\n");  

    RMBCnormal(G);
    G->nc = Greedy(G);
    printf("\nGreedy En orden de RMBCnormal: %u \n", G->nc);
    (check_colors(G) == 1) ? printf("Esta BIEN coloreado\n") : printf("Esta MAL coloreado\n");  
    
    RMBCrevierte(G);
    G->nc = Greedy(G);
    printf("\nGreedy En orden de RMBCrevierte: %u \n", G->nc);
    (check_colors(G) == 1) ? printf("Esta BIEN coloreado\n") : printf("Esta MAL coloreado\n");  

    RMBCchicogrande(G);
    printf("\n");
    G->nc = Greedy(G);
    printf("\nGreedy En orden de RMBCchicogrande: %u \n", G->nc);
    (check_colors(G) == 1) ? printf("Esta BIEN coloreado\n") : printf("Esta MAL coloreado\n");  
    
    int b = Bipartito(G);
    (b == 1) ? printf("Es bipartito \n") : printf("No es bipartito \n");
    (check_colors(G) == 1) ? printf("Esta BIEN coloreado\n") : printf("Esta MAL coloreado\n");   
    
    DestruccionDelGrafo(G);
}
   /* printf("------------------- EMPIEZA EL TEST DEL G2 ----------------\n");

    OrdenNatural(G2);
    Greedy(G2);
    printf("\nGreedy En orden de Natural: %u \n", G2->nc);
    (check_colors(G2) == 1) ? printf("Esta BIEN coloreado\n") : printf("Esta MAL coloreado\n");  

    OrdenWelshPowell(G2);
    Greedy(G2);
    printf("\nGreedy En orden de WellPower: %u \n", G2->nc);
    (check_colors(G2) == 1) ? printf("Esta BIEN coloreado\n") : printf("Esta MAL coloreado\n");  

    RMBCnormal(G2);
    G2->nc = Greedy(G2);
    printf("\nGreedy En orden de RMBCnormal: %u \n", G2->nc);
    (check_colors(G2) == 1) ? printf("Esta BIEN coloreado\n") : printf("Esta MAL coloreado\n");  
    
    RMBCrevierte(G2);
    G2->nc = Greedy(G2);
    printf("\nGreedy En orden de RMBCrevierte: %u \n", G2->nc);
    (check_colors(G2) == 1) ? printf("Esta BIEN coloreado\n") : printf("Esta MAL coloreado\n");  

    RMBCchicogrande(G2);
    printf("\n");
    G2->nc = Greedy(G2);
    printf("\nGreedy En orden de RMBCchicogrande: %u \n", G2->nc);
    (check_colors(G2) == 1) ? printf("Esta BIEN coloreado\n") : printf("Esta MAL coloreado\n");  
    
    b = Bipartito(G2);
    (b == 1) ? printf("Es bipartito \n") : printf("No es bipartito \n");
    (check_colors(G2) == 1) ? printf("Esta BIEN coloreado\n") : printf("Esta MAL coloreado\n");   
    
    DestruccionDelGrafo(G2);
    
    return 0;
}  
    printf("Orden viejo: \n");
    for (int i = 0; i < G2->vertices; i++)
    {
        printf("(%u, %u) ", NombreDelVertice(G, i), ColorDelVertice(G, i));
    }
    printf("\n");
*/
