typedef struct Blockes * blocke;
typedef struct Nodos * nodo;
typedef unsigned int u32;
struct Blockes{
    u32* array; //Vecinos
    int size; //Cantidad de elementos
    int cap; //Capacidad total de elementos
};


struct Nodos{
    u32* array; //Vecinos
    u32 size; //Cantidad de vecinos
    u32 cap; //Capacidad total de vecinos
    u32 color; //Color del nodo
    u32 name; //Nomber del nodo
};


nodo* create_nodo(u32 max); //Crea el nodo con todos los datos que tiene un nodo
void push_nodo(u32 neighbour, nodo v, u32 x); //Mete una key en una posicion del nodo
void destroy_nodo(u32** nodo); //Destruye el Nodo
blocke* create_vector(u32 max); //Crea el vector, arreglo de arreglos de u32
void push_blocke(blocke v, u32 x); //Crea el vector, arreglo de arreglos de u32
