#ifndef ORDEN_H
#define ORDEN_H
#include "Rii.h"


u32* create_orden(u32 max);
char OrdenNatural(Grafo G);
char OrdenWelshPowell(Grafo G);
char SwitchVertices(Grafo G, u32 i, u32 j);
char RMBCnormal(Grafo G);
char RMBCrevierte(Grafo G);
char RMBCchicogrande(Grafo G);
char SwitchColores(Grafo G, u32 i, u32 j);

#endif
