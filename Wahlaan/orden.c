#include "Rii.h"

//Asigna memoria al arreglo orden del grafo.
u32* create_orden(u32 max){
    u32* a = calloc(max ,sizeof(u32));
    return a;
}

//Intercambia las posiciones, la usamos para suapear las posiciones del arreglo orden.
void intercambio(u32 * array, u32 i, u32 j){
    u32 aux;
    aux = array[i];
    array[i] = array[j];
    array[j] = aux;
}

//Ordena los elementos de menor a mayor.
void quicksort(Grafo G, u32 inf, u32 sup){
    u32 i = inf;
    u32 j = sup;
    //Se asigna un pivote.
    u32 x = NombreDelVertice(G, (i+j)/2); 
    while(i <= j){ 
        //Obtenemos el elemento mayor de lado izquierdo del pivote a intercambiar 
        while(NombreDelVertice(G, i) < x)   
            i++;
        //Obtenemos el elemento menor de lado derecho del pivote a intercambiar
        while(NombreDelVertice(G, j) > x)   
            j--;
        //intercambiamos cuando se sigan presentando
        //valores menores a la derecha y mayores a la izquierda
        if(i <= j){             
            intercambio(G->orden, i, j);
            i++;
            if(j != 0)
                j--;
        }
    }
    //hacemos recursion aplicando divide y venceras.
    if(inf < j)
        quicksort(G, inf, j);

    if(i < sup)
        quicksort(G, i, sup);
}

//Ordena los vectores en orden natural usando quicksort
char OrdenNatural(Grafo G){
    quicksort(G, 0, NumeroDeVertices(G)-1);
    return 0;
}

//Ordena los vertices de mayor a menor con respecto a su grado.
char OrdenWelshPowell(Grafo G){
    //Asigno memoria a los bloques.
    blocke * blocks = create_blocke(NumeroDeVertices(G));
    //Si no pudo asignas retorna 1.
    if(!blocks) return 1;
    
    //Guardo en bloque la posicion de los nodos que tengan el mismo grado.
    for (u32 i = 0; i < NumeroDeVertices(G); i++){
        push_blocke(blocks[GradoDelVertice(G, i)], G->orden[i]);
    }
    
    //Asigno en el arreglo orden el nuevo orden de los vertices dado por los bloques.
    //como los bloques estan ordenados entre si, los vertices
    //tambien lo estaran con respecto a su grado.
    int k = 0;
    for (u32 i = NumeroDeVertices(G)-1; i < NumeroDeVertices(G); i--){
        for (int j = 0; j < blocks[i]->size; j++){
            G->orden[k] = blocks[i]->array[j];
            k++;
        }
    }

    //Libero la memoria ocupada por los bloques
    for (u32 i = 0; i < NumeroDeVertices(G); i++){
        free(blocks[i]->array);
        free(blocks[i]);
    }
    free(blocks);

    return 0;
}
//Intercambia las posiciones de los vertices i, j
//solo si existen dichos vertices.
char SwitchVertices(Grafo G, u32 i, u32 j){
    if(i > NumeroDeVertices(G) || j > NumeroDeVertices(G))
        return 1;

    u32 aux = G->orden[i];
    G->orden[i] = G->orden[j];
    G->orden[j] = aux;
    return 0;
}

char RMBCnormal(Grafo G){
	u32 nc = G->nc;
    //Asigno memoria a los bloques.
    blocke * blocks = create_blocke(nc);
    //Si no pudo asignas retorna 1.
    if(!blocks) return 1;
    nodo * vectores = G->vectores;
    
    //Guardo en el bloque la posicion de los vertices que tengan el mismo color.
    for (u32 i = 0; i < G->vertices; i++)
        push_blocke(blocks[vectores[G->orden[i]]->color], G->orden[i]);
    
    //Asigno en el arreglo orden el nuevo orden de los vertices dado 
    //por los bloques.
    //como los bloques estan ordenados entre si, los vertices
    //tambien lo estaran con respecto a su color.
    int k = 0;
    for (u32 i = 0; i < nc; i++){
        for (int j = 0; j < blocks[i]->size; j++){
            G->orden[k] = blocks[i]->array[j];
            k++;
        }
    }

    //Libero la memoria ocupada por los bloques
    for (u32 i = 0; i < nc; i++){
        free(blocks[i]->array);
        free(blocks[i]);
    }
    free(blocks);
    return 0;
}

char RMBCrevierte(Grafo G){
	u32 nc = G->nc;
	nodo * vectores = G->vectores;
    //Asigno memoria a los bloques.
    blocke * blocks = create_blocke(nc);
    //Si no pudo asignas retorna 1.
    if(!blocks) return 1;
    
    //Guardo en bloque la posicion de los vertices que tengan el mismo color.
    for (u32 i = 0; i < G->vertices; i++)
        push_blocke(blocks[vectores[G->orden[i]]->color], G->orden[i]);

    //Asigno en el arreglo Orden el nuevo orden de los vertices dado 
    //por los bloques asignados de manera opuesta.
    //como los bloques estan ordenados entre si, los vertices
    //tambien lo estaran con respecto a su color.
    //al asignarlo de manera opuesta quedan de mayor a menor.
    int k = 0;
    for (int i = nc-1; i >= 0; i--){
        for (int j = 0; j < blocks[i]->size; j++){
            G->orden[k] = blocks[i]->array[j];
            k++;
        }
    }

    //Libero la memoria ocupada por los bloques
    for (u32 i = 0; i < nc; i++){
        free(blocks[i]->array);
        free(blocks[i]);
    }
    free(blocks);
    
    return 0;
}

void sortBlocks(blocke * blocks, int inf, int sup){
    int i, j, x;
    i=inf;
    j=sup;

    //Se asigna un pivote.
    x = blocks[(i+j)/2]->size; 
    while(i <= j){ 
        //Obtenemos el elemento mayor de lado izquierdo del  pivote
        // a intercambiar
        while(blocks[i]->size < x)   
            i++;
        //Obtenemos el elemento menor de lado derecho del pivote
        // a intercambiar
        while(blocks[j]->size > x)   
            j--;
        //intercambiando cuando se sigan presentando
        //valores menores a la derecha y mayores ala izquierda
        if(i <= j){             
            blocke aux = blocks[i];
            blocks[i] = blocks[j];
            blocks[j] = aux;
            i++;
            if(j != 0)
                j--;
        }
    }
    //hacemos recursion aplicando divide y venceras.
    if(inf < j)
        sortBlocks(blocks, inf, j);
    if(i < sup)
        sortBlocks(blocks, i, sup);
}

//Ordena por tamaño de los bloques
char RMBCchicogrande(Grafo G){
	u32 nc = G->nc;
	nodo* vectores = G->vectores;
    blocke * blocks = create_blocke(nc);
    if(!blocks) return 1;
    
    //Guardo en bloques la posicion de los nodos que tengan el mismo color
    for (u32 i = 0; i < G->vertices; i++)
        push_blocke(blocks[vectores[G->orden[i]]->color], G->orden[i]);

    //Ordeno por el la cantidad de vertices que tenga cada bloque
    //de cada color, se usa un algorimo de tipo quicksort
    sortBlocks(blocks, 0, nc-1);    
    
    //Asigna en el arreglo orden el nuevo orden de los vertices dado 
    //por los bloques.
    //como los bloques estan ordenados entre si, los vertices
    //tambien lo estaran.
    int k = 0;
    for (u32 i = 0; i < nc; i++){
        for (int j = 0; j < blocks[i]->size; j++){
            G->orden[k] = blocks[i]->array[j];
            k++;
        }

    }

    //Libero la memoria ocupada por los bloques
    for (u32 i = 0; i < nc; i++){
        free(blocks[i]->array);
        free(blocks[i]);
    }
    free(blocks);
    //Retorno 0 si todo sale bien.
    return 0;
}

//Remplaza los colores de todos los vertices con el color i por el j.
//y todos los con el color j por el i.
char SwitchColores(Grafo G, u32 i, u32 j){
	
	nodo* vectores = G->vectores;
    //Confirmo que los colores sean validos.
    if(i > G->nc || j > G->nc)
        return 1;
    u32 a = i, b = j;

    //Recorro todos los vertices y remplazo los colores
    //de los debidos.
    for (u32 i = 0; i < G->vertices; i++){
        if(vectores[G->orden[i]]->color == a)
            vectores[G->orden[i]]->color = b;
        else if(vectores[G->orden[i]]->color == b)
            vectores[G->orden[i]]->color = a;
    }
    //Retorno 0 si todo sale bien.
    return 0;
}