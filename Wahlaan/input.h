#ifndef INPUT_H
#define INPUT_H
#include "Rii.h"

char* input_line(char* linea);//Carga una linea por el standar input de un archivo 
char* search_p_edge(char*); //Busca la linea que contiene el p edge y retona esa misma linea
u32 search_number(u32 i, char* a, int option);
u32 n_aristas(char * string); //Retorna la cantidad de aristas
u32 n_vertices(char * string); //Retorna la cantidad de vertices

#endif