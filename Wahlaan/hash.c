#include "Rii.h"

//Creo la estructura del hash
dat dat_create(u32 max) {
	//Inicializo rand() con el clock de la pc.
	srand(time(NULL));
	//Asigno memoria a la estructura.
    dat d = (dat)malloc(sizeof(struct datos));
    //Confirmo el alloc.
    if(d == NULL){ 
    	printf("No se pudo asignar memoria al hash\n");
    	return NULL;
    }
    //Asigno memoria al arreglo de posiciones.	
    d->array = (u32*)malloc(sizeof(u32)*max*3);
    //Confirmo el alloc.
    if(d == NULL){ 
    	printf("No se pudo asignar memoria al  array de hash\n");
    	free(d);
    	return NULL;
    }
    //Inicializo el array en MAXNUM.
    for(u32 i = 0 ; i < max*3; i++)
    	d->array[i]= MAXNUM;
    //asigno el tamaño del hash.
    d->size = max*3;
    //Contadores de coliciones.
    d->k = 0;
    d->coliciones = 0;
    d->colicionesM = 0;
    //Genero 2 numeros random para la funcion hash.
    d->u = max*2 + rand()%(max*2);
    d->t = max*4 + rand()%(max*4);

    return d;
}

//Esta es nuestra funcion Hash.
u32 function(u32 key, dat d){
    u32 max = d->size;
    u32 num = key;
    //Se multiplica la key * uno de los numeros random  
    //y se le sumo a la key * el otro numero random
    //luego se los suma y se le hace modulo.
    num = (num*(d->t) + num*(d->u)) % max;
    	
    return num;
}

//Guarda el la key en el array si es necesario y devuelve su posicion
u32 push_dat(dat d, u32 key){
	//Se le aplica la funcion a la key.
    u32 pos = function(key, d);
    u32 max = d->size;
    u32 coliciones = 0;

    //Si la posicion de la key esta ocupada y no es pos la key mismo
    //se pasa a la siguiente posicion y se cuenta  la colicion.
    while(d->array[pos] != MAXNUM && d->array[pos] != key){
        pos++;
        pos = pos % max;
        coliciones++;
    }
    //En casa de que ya esista la key en el array se retorna
    //size+1 para que la carga no asigne el vector.
    if(d->array[pos] == key){
        return d->size+1;
    } 

    //Cuando se encontra un lugar libre para la key
    //se la asigna al lugar y retorna su posicion.
    else{
        d->array[pos] = key;
        //Cuenta las coliciones.
        d->coliciones += coliciones;
        if(coliciones > 0)
            d->k++;
        if(d->colicionesM < coliciones)
            d->colicionesM = coliciones;
    } 
    //Retorna la posicion de la key.
    return pos;
}

//Busca la posicion de la key que ya exista en el array.
u32 search_dat(dat d, u32 key){
	//Se le aplica la funcion a la key.
    u32 pos = function(key, d);
    u32 max = d->size;
    //Busca la posicion de la key.
    while(d->array[pos] != key){
        pos++;
        pos = pos % max;
    }
    //Retorna la posicion de la key. 
    return pos;
}


