#include "Rii.h"

typedef struct _stack_t * Pila;


u32 NumeroDeVertices(Grafo G);
u32 NumeroDeLados(Grafo G);
u32 NumeroDeColores(Grafo G);
u32 NombreDelVertice(Grafo G, u32 i);
u32 ColorDelVertice(Grafo G, u32 i);
u32 GradoDelVertice(Grafo G, u32 i);
u32 ColorJotaesimoVecino(Grafo G, u32 i,u32 j);
u32 NombreJotaesimoVecino(Grafo G, u32 i,u32 j);
u32 Greedy(Grafo G);
u32 Bipartito(Grafo G);
u32 check_colors(Grafo G);