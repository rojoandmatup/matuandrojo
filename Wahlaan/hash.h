typedef struct datos * dat;
typedef unsigned int u32;
//Structura de mi Hash
struct datos{
    u32* array;
    u32 size;
    u32 coliciones;
    u32 colicionesM;
    u32 k;
    u32 u;
    u32 t;
};


dat dat_create(u32 MAX); //Inicializa el hash para mapear los vertices
u32 function(u32 key, dat d); //Funcion de Hash para buscar las aristas dada una key
u32 push_dat(dat d, u32 key); //Mete un dato en la tabla de hash
u32 search_dat(dat d, u32 key); //Busca un dato en la tabla dada una key
