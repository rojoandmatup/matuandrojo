#include "Rii.h"

blocke* create_blocke(u32 max){
    //Damos memoria para los blockes para algunos ordenamientos.
    blocke* b = (blocke*)malloc(sizeof(struct Blockes) * max);
    if(b == NULL){
        printf("Error al asignar memoria a los Blockes\n");
        return NULL;
    }

    //Asigno memoria a cada bloque.
    for(u32 i = 0; i < max; i++){
        b[i] = (blocke)malloc(sizeof(struct Blockes));

        //Confirmo el alloc.
        if(b[i] == NULL){
            printf("Error al asignar memoria al blocke %u\n", i);
            for(u32 j = 0; j < i; j++){
                free(b[j]);
            }
            free(b);
        }

        //Inicializo el blocke.
        b[i]->array = NULL;
        b[i]->size = 0;
        b[i]->cap = 0;
    }
    //Retorno los BLockes
    return b;
}

//Damos memoria para los nodos.
nodo* create_nodo(u32 max){
    nodo* v = calloc(max ,sizeof(struct Nodos));
    return v;
}

//Realloco la memoria del array de un vector.
void memory_neighbour(nodo v, u32 cap){
    v->array = (u32*)realloc(v->array, cap * sizeof(u32));
}

//Asigno la capacidad y los vecinos a cada vector.
void push_nodo(u32 neighbour, nodo v, u32 myName){
    //Si la capacidad es igual a la cantidad de vecinos actuales.
    //duplico la capacidad y agrego el vecino nuevo.
    if(v->cap == v->size){
        if(v->cap == 0){
            v->cap = 2;
            v->array = calloc(2 ,sizeof(nodo));
        }
        else {
            memory_neighbour(v, 2*v->cap);
            v->cap *= 2;
        }
    }
    //Asigno el vecino y aumento el contador de vecinos.
    v->array[v->size] = neighbour;
    v->name = myName;
    v->size++;
}

//Realloco la capacidad del array de un bloque.
void memory_vector(blocke v, u32 cap){
    v->array = (u32*)realloc(v->array, cap * sizeof(u32));
}

//Asigno la cantidad de vertices en el blocke
void push_blocke(blocke v, u32 x){
    //Si la capacidad es igual a la cantidad de vertices
    //duplico la capacidad de vertices y lo asignoS.
    if(v->cap == v->size){
        if(v->cap == 0) v->cap = 1;
        memory_vector(v, 2*v->cap);
        v->cap *= 2;
    }
    v->array[v->size] = x;
    v->size++;
}
