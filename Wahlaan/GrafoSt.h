#ifndef GRAFOST_H
#define GRAFOST_H

typedef struct GrafoSt * Grafo;
#include "Rii.h"

struct GrafoSt{
    u32 vertices; // numero de los vertices. (Nodos)
    u32 aristas; // numeros de los aristas. (Union entre nodos)
    dat hash; // Aca se resuelve el problema de los nombres
    u32 nc; // cantidad de colores.
    u32* orden; // Orden de los vertices
    nodo* vectores; //Contiene toda la informacion que tiene cada nodo (nombre, vecinos, color, etc)
};

Grafo ConstruccionDelGrafo();
void DestruccionDelGrafo(Grafo G);

#endif