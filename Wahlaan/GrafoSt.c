#include "Rii.h"

//Cargan los vectores y retorna cuantos vertices cargo
u32 carga(u32* vertices, Grafo G, u32 i){
    u32 exist = G->hash->size+1; //Variable para detectar un vertice ya cargado.
    u32 x = 0; //Cuenta los vertices que se cargaron.
    u32 pos[2] = {0, 0};  //Guarda las posiciones de los vertices.
    u32 v = 0; //Variable para cargar los 2 vertices.

    while(v != 2){ //Aplica la funcion hash a cada vertice de la linea.
        pos[v] = push_dat(G->hash, vertices[v]); //Retorna la posicion donde se va a crear el vector.
        if(exist - pos[v]){  //Si el vector debe ser cargado, la resta es Mayor a 0
            // Se guarda en el orden, la posicion donde estara el vector en G->vectores[pos] 
            G->orden[i] = pos[v]; 
            i = i + 1;
            x++;
        }
        else //Si el vector ya exitia se busca su posicion. 
            pos[v] = search_dat(G->hash, vertices[v]); 

        //Si el vector aun no existe se lo crea.
        if(G->vectores[pos[v]] == NULL){ 
            G->vectores[pos[v]] = (nodo)malloc(sizeof(struct Nodos));
            G->vectores[pos[v]]->array = (u32*)malloc(sizeof(u32)*2);
            G->vectores[pos[v]]->color = vertices[v]; //Su color es su nombre.
            G->vectores[pos[v]]->size = 0; //Cantidad de vecinos.
            G->vectores[pos[v]]->cap = 2; //Capacidad actual de vecinos.
            G->vectores[pos[v]]->name = vertices[v];  //Se le asigna su nombre.
        }
        v++;
    }
	//Asigna sus vecinos a cada vector.
    push_nodo(pos[1], G->vectores[pos[0]], vertices[0]);
    push_nodo(pos[0], G->vectores[pos[1]], vertices[1]);

    //Retorna la cantidad que fueron cargados.
    return x; 
}


void DestruccionDelGrafo(Grafo G){
	//Confirmo que exista G
	if(G != NULL){
		//Confirmo que existan los vectores.
		if(G->vectores != NULL) {
		    for(u32 i = 0; i < NumeroDeVertices(G); i++){
		    	 //Confirmo que exista la posicion.
                if(G->vectores[G->orden[i]] != NULL){
                    if(G->vectores[G->orden[i]]->array != NULL)
                        free(G->vectores[G->orden[i]]->array);
                    free(G->vectores[G->orden[i]]);
                }
		    }
		    free(G->vectores);
            G->vectores = NULL;
		}

		//Confirmo que existan el orden.
		if(G->orden != NULL){
	    	free(G->orden);
            G->orden = NULL;
        }
	    free(G);
        G = NULL;
	}    
}


Grafo ConstruccionDelGrafo(){

    //Alloca en memoria el grafo
    Grafo G = calloc(1, sizeof(struct GrafoSt));

    //Alloca memoria para la linea Pedge.
    char* pedge = calloc(80 ,sizeof(char));

    //Busca la linea Pedge y la asigna.
    pedge = search_p_edge(pedge);

    //Si No la encontro la libera y retorna NUll.
    if(pedge == NULL){ 
        free(G);
        printf("Fallo al allocar pedge\n");
        return NULL;
    }

    G->vertices = n_vertices(pedge); //Carga la cantidad de vertices.
    G->aristas = n_aristas(pedge); //Carga la cantidad de aristas.

    //Asignamos la cantidad de Colores = Vertices.
    G->nc = G->vertices; 

    free(pedge);

    //Asignamos memoria a hash.
    G->hash = dat_create(G->vertices);
    G->hash->coliciones = 0;

    if(G->hash == NULL){
    	free(G);
    	printf("Fallo al allocar hash\n");
    	return NULL;
    }

    //Asigna memoria al arreglo orden.
    G->orden = create_orden(G->vertices);

    //Si no pudo asignar libera la memoria y retorna NULL.
    if(G->orden == NULL){
    	free(G->hash->array);
    	free(G->hash);
    	free(G);
    	printf("Fallo al allocar orden\n");
    	return NULL;
    }

    //Asigna memoria a los vectores.
    G->vectores = create_nodo(G->hash->size);

    //SI no pudo asignar libera la memoria y retorna NULL.
    if(G->vectores == NULL){
    	free(G->hash->array);
    	free(G->hash);
    	free(G->orden);
    	free(G);
    	printf("Fallo al allocar vectores\n");
    	return NULL;
    }

    //Arreglo donde guardaremos los valores x y de la linea e x y
    u32 * vertices = calloc(2 ,sizeof(u32));

    u32 k = 0;
    u32 x = 0;

    //Allocamos memoria para una linea.
    char * line = calloc(80 ,sizeof(char));

    //Si no pudo asignar libera la memoria y retorna NULL.
    if(line == NULL){
    	free(G->hash->array);
    	free(G->hash);
    	free(G->orden);
    	free(G->vectores);
    	free(G);
        free(vertices);
    	printf("Fallo al allocar line\n");
    	return NULL;
    }

    //Cargamos todas las lineas y allocamos todos los vectores con sus vecinos.
    for(u32 i = 0; i < NumeroDeLados(G); i++) {
        //Cargamos la linea.
        line = input_line(line); 
        if(!line) {
            printf("Error al cargar linea\n");
            free(G->hash->array);
    		free(G->hash);
    		free(line);
    		free(vertices);
    		DestruccionDelGrafo(G);
            return NULL;
        }

        //Compara que la linea sea correcta
        if(line[0] != 'e' || line[1] != ' '){
            printf("Error de lectura en lado %i\n", i + 1);
            free(G->hash->array);
    		free(G->hash);
    		free(line);
    		free(vertices);
    		DestruccionDelGrafo(G);
            return NULL;
        }

        //Guardamos el primer numero de la linea.
        vertices[0] = search_number(2, line, 1);

        //Guardamos el segundo numero de la linea.
        vertices[1] = search_number(2, line, 2);

        //Cargamos los vertices.
        x = carga(vertices, G, k);
        k += x;     
    }

    //Si la cantidad de vertices cargados no son los 
    //que seberian ser, se libera la memoria y
    //se retorna NULL.
   	if(k != NumeroDeVertices(G)){
   		printf("Error cantidad de vectores leidos no es la declarada\n");
   		free(G->hash->array);
    	free(G->hash);
    	free(line);
    	free(vertices);
    	DestruccionDelGrafo(G);
    	return NULL;
   	}

   	//Imprimimos las coliciones.
    printf("\nHAY: %u COLICIONES NUMERICAS\n", G->hash->k);
    printf("\nHAY: %u COLICIONES TOTALES\n", G->hash->coliciones);
    printf("LA MAXIMA COLICION EN 1 ELEMENTO ES: %u\n\n", G->hash->colicionesM);

    //Liberamos hash y line.
    free(G->hash->array);
    free(G->hash);
    free(line);
    free(vertices);

    return G;
}

//Copia el Grafo.
Grafo CopiarGrafo(Grafo G){
    //Alloca memoria al nuevo grafo.
    Grafo G2 = calloc(1, sizeof(struct GrafoSt));

    //Comprueba alloc.
    if (G2 == NULL) return NULL; 

    //Copia los datos: cantidad de nodos, lados y colores.
    u32 n = G->vertices;
    u32 m = G->aristas;
    u32 nc = G->nc;
    G2->vertices = n;
    G2->aristas = m;
    G2->nc = nc;

    //Asigna memoria al orden
    G2->orden = create_orden(n);

    //Comprueba alloc.
    if(G2->orden == NULL){
        free(G2);
        return NULL;
    } 
    //Asigna memoria a los vectores.
    G2->vectores = create_nodo(n*3);

    //Comprueba alloc.
    if(G2->vectores == NULL){
        free(G2->orden);
        free(G2);
        return NULL;
    }   

    //Copia el orden y la estructura de los nodos con sus vecinos
    for (u32 i = 0; i < n; i++){
        //Copia el orden
        u32 o = G->orden[i];
        G2->orden[i] = o;

        u32 pos = G2->orden[i];

        //Crea el nodo y copia los datos del nodo: cantidad de vecinos, el color del nodo y el nombre del nodo
        u32 size = G->vectores[pos]->size;
        u32 color = G->vectores[pos]->color;
        u32 name = G->vectores[pos]->name;
        G2->vectores[pos] = (nodo)malloc(sizeof(struct Nodos));
        G2->vectores[pos]->array = (u32*)malloc(sizeof(u32)*size);
        G2->vectores[pos]->size = size;
        G2->vectores[pos]->cap = size;
        G2->vectores[pos]->color = color;
        G2->vectores[pos]->name = name;

        //Copia los vecinos del nodo.
        for (u32 j = 0; j < size; j++){
            u32 vecino = G->vectores[pos]->array[j];
            G2->vectores[pos]->array[j] = vecino;
        }

    }  

    return G2;
}