/*
    Matias Pereyra - matup98@gmail.com
    Lucas Caballero - lucaserfurt@hotmail.com
*/

#ifndef RII_H
#define RII_H

//Definiciones
typedef unsigned int u32;
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "vectores.h"
#include "hash.h"
#include "GrafoSt.h"
#include "input.h"
#include "orden.h"
#include "colores.h"
#define MAXNUM 4294967295

//Inputs
char* input_line(char*); //Carga una linea por el standar input de un archivo 
char* search_p_edge(); //Busca la linea que contiene el p edge y retona esa misma linea
u32* charge_aristas(); //Retorna las aristas del input (e n m)
u32 search_number(u32 i, char* a, int option);
u32 n_aristas(char * string); //Retorna la cantidad de aristas
u32 n_vertices(char * string); //Retorna la cantidad de vertices

//Grafos
Grafo ConstruccionDelGrafo();
void DestruccionDelGrafo(Grafo G);
Grafo CopiarGrafo(Grafo G);

//Orden
u32* create_orden(u32 max);
char OrdenNatural(Grafo G);
char OrdenWelshPowell(Grafo G);
char SwitchVertices(Grafo G, u32 i, u32 j);
char RMBCnormal(Grafo G);
char RMBCrevierte(Grafo G);
char RMBCchicogrande(Grafo G);
char SwitchColores(Grafo G, u32 i, u32 j);

//Colores
u32 NumeroDeVertices(Grafo G);
u32 NumeroDeLados(Grafo G);
u32 NumeroDeColores(Grafo G);
u32 NombreDelVertice(Grafo G, u32 i);
u32 ColorDelVertice(Grafo G, u32 i);
u32 GradoDelVertice(Grafo G, u32 i);
u32 ColorJotaesimoVecino(Grafo G, u32 i, u32 j);
u32 NombreJotaesimoVecino(Grafo G, u32 i, u32 j);
u32 Bipartito(Grafo G);
u32 Greedy(Grafo G);
u32 check_colors(Grafo G);

//vectores
nodo* create_nodo(u32 max); 
void push_nodo(u32 neighbour, nodo v, u32 x);
blocke* create_blocke(u32 max);
void push_blocke(blocke v, u32 x);

#endif


