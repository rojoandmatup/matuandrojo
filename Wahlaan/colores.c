#include "Rii.h"

///////////////////////STACK/////////////////////

//Defino stacks para bipartito//
typedef struct Bloque * bloques;

struct Bloque {
    nodo elem;
    bloques next;
};

typedef struct _stack_t {
    bloques stack;
} _stack_t;

//Crea la pila.
Pila create_stack() {
    Pila s = calloc(1,sizeof(_stack_t));
    s->stack = NULL;
    return (s);
}

//Agrega un elemento nuevo a la pila.
void push(nodo  e, Pila s) {
    bloques node = calloc(1,sizeof(struct Bloque));
    node->next = s->stack;
    node->elem = e;
    s->stack = node;
}

//Retorna 1 si la pila esta vacia.
u32 is_empty_stack(Pila s) {
    if(s->stack == NULL)
    	return 1;
    return 0;
}

//Retira el primer elemento de la pila.
void pop(Pila s) {
    if(!is_empty_stack(s)){
    	bloques node = s->stack;
    	s->stack = s->stack->next;
    	free(node);
    	node = NULL;	
	}
}

//Destruye la pila.
void destroy_stack(Pila s) {
    while (!is_empty_stack(s)) {
        pop(s);
    }
    free(s);
}

///////////////////FIN STACK////////////////////



//Retorna la cantidad de vertices.
u32 NumeroDeVertices(Grafo G){
    return G->vertices;
}

//Retorna la cantidad de aristas.
u32 NumeroDeLados(Grafo G){
    return G->aristas;
}

//Retorna la cantidad de colores.
u32 NumeroDeColores(Grafo G){
    return G->nc;
}

//Retorna el nombre del vertice i.
u32 NombreDelVertice(Grafo G, u32 i){
    return G->vectores[G->orden[i]]->name;
}

//Retorna el color del vertice i.
u32 ColorDelVertice(Grafo G, u32 i){
    //Si el vertice no existe Error.
    if(i >= G->vertices) 
        return MAXNUM;

    u32 pos = G->orden[i];
    u32 color = G->vectores[pos]->color;
    return color;
}

//Retorna el Grado del vertice i.
u32 GradoDelVertice(Grafo G, u32 i){ 
    //Si el vertice no existe Error.
    if(i >= G->vertices)
        return MAXNUM;

    u32 pos = G->orden[i];
    u32 grado = G->vectores[pos]->size;
    return grado;

}

//Retorna el color del vecino j del vertice i . 
u32 ColorJotaesimoVecino(Grafo G, u32 i, u32 j){
    u32 pos = G->orden[i];

    //Si el vertice o el vecino no existe Error.
    if(i >= G->vertices || j >= G->vectores[pos]->size) 
        return MAXNUM; 

    u32 posV = G->vectores[pos]->array[j];

    u32 color = G->vectores[posV]->color;
    return color;    
}

//Retorna el nombre del vecino j del vertice i. 
u32 NombreJotaesimoVecino(Grafo G, u32 i,u32 j){
    u32 pos = G->orden[i];
    u32 posV = G->vectores[pos]->array[j];
    u32 nombre = G->vectores[posV]->name;
    return nombre;
}

//Inicializa los colores y el arreglos para Greedy.
void set_color(Grafo G, u32 * using){
    for(u32 i = 0 ; i < G->vertices; i++){
        using[i] = MAXNUM;
        G->vectores[G->orden[i]]->color = MAXNUM;
    }
}

//Inicializa los colores para Bipartito.
void set_init(Grafo G){
    for(u32 i = 0; i < G->vertices; i++){
        G->vectores[G->orden[i]]->color = MAXNUM;
    }
}


//Para Greedy usamos 2 arreglos, "Col" y "Using"
//El arreglo "Col" es para marcar los colores ocupados.
//El arreglo "using" es para poder limpiar el arreglo col
//en los caso necesarios unicamente.
//Se retorna Cantidad+1 porque el primer color (el 0)
//no es contado por nuestra variable cantidad

u32 Greedy(Grafo G) {
    u32 pos = 0, k = 0;
    u32 cantidad = 0; //Cuenta la cantida de colores usados.
    u32 colorVecino = MAXNUM;
    u32 vertices = G->vertices;
    nodo* vectores = G->vectores;
    //Arreglo para marcar colores ocupados. 
    u32* col = (u32*)calloc(vertices, sizeof(u32));
    //Arreglo para guardar los colores usados.
    u32* using = (u32*)malloc(vertices* sizeof(u32));
    //Inicializa los colores y el arreglo using.
    set_color(G, using);
    //Recorro todos los vertices.
    for(u32 i = 0; i < vertices; i++){
        k = 0;
        pos = G->orden[i];
        //Recorro todos los vecinos.
        for(u32 j = 0; j < vectores[pos]->size; j++){
        	//Guardo el color del vecino j. 
            colorVecino = vectores[vectores[pos]->array[j]]->color;
            //Nos fijamos si el vecino tiene color
            //y no fue marcado como ocupado.
            if(colorVecino != MAXNUM && !col[colorVecino]){
                //Marcamos el color ocupado con un 1
                //en el arreglo col.
                col[colorVecino] = 1;
                //Guardamos el color como usado.
                using[k] = colorVecino;
                //Incrementamos el contador de usados.
                k++;
            }
        }

        k = 0;
        //Se busca el primer color no ocupado.
        while(col[k] == 1){
            k++;
        }    
        //Se guarda el color en el vertice.
        vectores[pos]->color = k;
        //Si el color es mas grande que el contador se incrementa.
        if(cantidad < k) cantidad++;

        k = 0;
        //Buscamos todos los colores usados para limpiar
        //el arreglo de colores ocupados
        while(using[k] != MAXNUM){
        	//Limpia los numeros ocupados.
            col[using[k]] = 0;
            //Limpia el arreglo de usados.
            using[k] = MAXNUM;
            k++;
        }
    }
    
    //Libero los arreglos.
    free(using);
    free(col);
    
    G->nc = cantidad+1;
    //Retorno la cantidad de colores usados.
    return cantidad+1;
}


//Recorremos el Grafo con "BFS" mediante pilas.
u32 Bipartito(Grafo G){
    //Se inicializan los colores del grafo.
    set_init(G);
    //Variable de color.
    u32 color = 0;
    u32 pos = 0;
    //Se crea el stack.
    Pila p = create_stack();
    nodo* vectores = G->vectores;
    //Recorremos todo el grafo hasta que este completamente coloreado
    //o no sea bipartito.
    for(u32 i = 0; i < G->vertices; i++){
    	pos = G->orden[i];
        //Si el vertice no tiene color se lo colorea y se lo agrega a la pila.
        if(vectores[pos]->color == MAXNUM){
            vectores[pos]->color = color;
            push(vectores[pos], p);
            //Mientras la pila tenga elementos se continua el BFS.
            while(!is_empty_stack(p)){
                //Guardo el vertice en un aux, para saber en cual recorrer los vecinos. 
                nodo aux = p->stack->elem;
                //Guardo su color
                color = aux->color;
                //Saco el vertice ya coloredo de la pila.
                pop(p);
                //Recorro todos los vecinos.
                for(u32 j = 0; j < aux->size; j++){
                    u32 posV = aux->array[j];
                    //Si el vecino no tiene color, lo coloreo y lo agrego a la pila.
                    if(vectores[posV]->color == MAXNUM){ 
                        vectores[posV]->color = (color+1)%2;
                        push(vectores[posV], p);
                    }
                    //Si el vecino tiene color y es el mismo que el del vertice
                    //No es bipartito por lo que se destrulle la pila, se llama a greedy y se retorna 0.
                    else if(vectores[posV]->color == color){
                        destroy_stack(p);
                        Greedy(G);
                        return 0;                    
                    }
                    //Si el vecino esta coloreado y es el opuesto al vertice, entonces ya lo recorri y no lo agrego a la pila.
                }
            }
        }
    }
    //Si el grafo es bipartito, se destruye la pila y se retorna 1        
    destroy_stack(p);
    G->nc = 2;
    return 1;    
}


//Funcion para chequear que ningun vertice tenga el mismo
//color del vecino.
u32 check_colors(Grafo G){
	u32 pos = 0;
    //Recorro los vertices
    for(u32 i = 0; i < NumeroDeVertices(G); i++){
        pos = G->orden[i];
        //Compruebo que no tenga el mismo color que alguno de sus vecinos.
        for(u32 j = 0; G->vectores[pos]->size > j; j++)
            if(ColorDelVertice(G, i) == ColorJotaesimoVecino(G, i, j))
                return 0;
    }
    return 1;
}