#include "Rii.h"

//Obtiene una linea.
char* input_line(char* line) {
    size_t sz = 0;

    char c;
    //Obtiena la linea caracter por caracter.
    while(EOF != (c = fgetc(stdin)) && c != '\n') {
        line[sz++] = c;
    }
    //Agrega el fin de linea.
    line[sz++] = '\0';
    //retorna la linea.
    return line;
}

//Transforma un char a un u32.
//Del caracter '2' al entero 2.
u32 ctoi(char string){ 
    u32 num = string - '0';
    return num;
}
    
//Identifica los numeros de la linea cargada "e n m".  
//Si la opcion es 1 optiene la n, si la opcion es 2 obtiene la m
u32 search_number(u32 i, char* a, int option){ 
//La option busca el primer o segundo numero.

    u32 number = 0;
    //Si option es 2, nos salteamos el primer numero de la linea 
    if(option == 2){
        //Nos movemos mientras exista una espacio
        while(a[i] == ' ')
    	    i++;
        //Nos movemos mientras exista un numero.
        while(a[i] != ' ')
    	    i++;
    }

    //Nos movemos mientras exista un espacio.
    while(a[i] == ' ')
        i++;
    
    //Vamos cargando el numero.
    while(a[i] != ' ' && a[i] != '\0'){
        //Transformamos cada caracter en un u32.
        u32 aux = ctoi(a[i]);
        //Sumamos el nuevo caracter al anterior *10.
        number = number * 10 + aux;
        i++;
    }

    //retorno el numero.
    return number;
}

//Obtiene la x de la linea "pedge x y"
u32 n_vertices(char * string){
	char * a = string;
    int i = 7;
    //Carga el primer numero de la linea p edge.
    u32 vertices = search_number(i, a, 1);
    return vertices;
}

//Obtiene la y de la linea "pedge x y"
u32 n_aristas(char * string){
    char * a = string;
    int i = 7;
    u32 lados = 0;
    //Carga el segundo numero de la linea p edge.
    lados = search_number(i, a, 2);
    return lados;
}

//Carga la linea p edge.
char* search_p_edge(char* pedge){
    //Cargamos una linea, luego vemos si es comentario, si es comentario, cargamos otra linea
    //sino es comentario comprobamos que este bien escrita...
    pedge = input_line(pedge);
    while(pedge != NULL && pedge[0] == 'c'){
        if(!pedge) {
            free(pedge);
            printf("Fallo al cargar la linea p edge\n");
            return NULL;
        }
        pedge = input_line(pedge);
    }

    char cmp[6] = {'p', ' ', 'e', 'd', 'g', 'e'};
    int sum = 0;

    //Comprobamos que realmente sea la linea p edge.
    for(int i = 0; i < 6; i++){
        if(cmp[i] == pedge[i]){
            sum++;
        }
        else break;
    }

    //Si la linea no es p edge printeamos un error y retornamos NULL.    
    if(sum != 6){
    	free(pedge);
        printf("Error en la primera linea sin comentarios 'c'\n");
    	return NULL;
    }

    //Retornamos la linea p edge.
    return pedge;
}
