memory:
	gcc -IWahlaan -Wall -Wextra -O3 -std=c99 ./Wahlaan/*.c main.c -o run

gdb:
	gcc -g -IWahlaan -Wall -Wextra -O3 -std=c99 ./Wahlaan/*.c main.c -o run
	gdb ./run

clean:
	rm -r ./run *.out